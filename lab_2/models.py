from django.db import models

# Create your models here.

class Note(models.Model):
    to = models.CharField(max_length=30, null=True)
    From = models.CharField(max_length=30, null=True)
    title = models.CharField(max_length=60, null=True)
    message = models.TextField(null=True)