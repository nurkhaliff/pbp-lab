1) Apakah perbedaan antara JSON dan XML?
JSON, seperti namanya, berbasis JavaScript, sedangkan XML berasal dari SGML.
JSON merepresentasikan data layaknya object, sementara XML adalah sebuah markup
Language dan menggunakan struktur tag untuk merepresentasikan data.
JSON lebih mudah dibaca oleh manusia ketimbang XML. JSON juga tidak
mendukung penggunakan encoding lain selain UTF-8, sementara XML
bisa menggunakan berbagai encoding. Dari segi keamanan, JSON lebih
rentan karena bisa disalahgunakan melalui metode yang berlaku untuk
JavaScript.

2) Apakah perbedaan antara HTML dan XML?
Perbedaan paling mencolok antara HTML dan XML terletak pada fungsinya. HTML lebih
berfokus kepada bagaimana menampilkan data dan bagaimana data tersebut ditampilkan (style), sementara XML lebih digunakan untuk menyimpan dan memindahkan data secara universal.