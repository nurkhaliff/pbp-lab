from django.urls import path
from .views import add_note, index, note_list, tutorial

urlpatterns = [
    path('', index, name='index'),
    path('tutorial/', tutorial, name='tutorial'),
    path('add-note/', add_note, name='add_note'),
    path('note-list/', note_list, name='note_list')
]
